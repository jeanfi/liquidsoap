# liquidsoap docker image

Docker image for running liquidsoap.

## Build image

```bash
cd docker
./build
```

## Usage

Execute a liquidsoap file:

```bash
cat ./example.liq | docker run -i --rm jeanfi/liquidsoap liquidsoap -
```
